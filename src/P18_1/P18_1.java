/**
 * Cameron Harris
 * Assignment 10
 */
package P18_1;

import static P18_1.PairUtil.minMax;

public class P18_1 {
    /**
     * Creates an array of type Measurable and calls the generic method minMax
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Measurable[] objs = {
                new Measurable() {
                    @Override
                    public double getMeasure() {
                        return 3;
                    }
                    }, new Measurable() {
                    @Override
                    public double getMeasure() {
                        return 5.5;
                    }
                    }, new Measurable() {
                    @Override
                    public double getMeasure() {
                        return 99.0;
                    }
                    }, new Measurable() {
                    @Override
                    public double getMeasure() {
                        return 1;
                    }
                }
        };
        //Gets pair object from minMax method
        Pair<Measurable, Measurable> minmax = minMax(objs);
        //Prints pair values (min, max)
        System.out.println("(" + minmax.getFirst().getMeasure() + ", " + minmax.getSecond().getMeasure() + ")");
    }
}
