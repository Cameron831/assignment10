/**
 * Cameron Harris
 * Assignment 10
 */
package P18_1;

public class PairUtil {
    /**
     * A generic method that computes the min and max values of an array type T
     *
     * @param objs Array of type T
     * @return Pair containing the min and max values
     */
    public static <T extends Measurable> Pair<T, T> minMax(T[] objs) {
        T min = objs[0];
        T max = objs[0];
        for (int i = 0; i < objs.length; i++) {
            if (objs[i].getMeasure() < min.getMeasure()) {
                min = objs[i];
            }
            if (objs[i].getMeasure() > max.getMeasure()) {
                max = objs[i];
            }
        }
        return new Pair<T, T>(min, max);
    }
}

        /*
        I first implemented the method as non-generic then converted it to generic

        public static Pair<Integer, Integer> minMax(ArrayList<Integer> ints) {
            int min = ints.get(0);
            int max = ints.get(0);
            for(int i = 0; i < ints.size(); i++) {
                if(ints.get(i) <= 0) {
                    min = ints.get(i);
                }
                if(ints.get(i) > 0) {
                    max = ints.get(i);
                }
            }
            return new Pair<>(min, max);
        }
        public static void main(String[] args) {
            ArrayList<Integer> ints = new ArrayList<Integer>();
            ints.add(3);
            ints.add(5);
            ints.add(0);
            Pair<Integer, Integer> minmax = minMax(ints);
        }
         */
