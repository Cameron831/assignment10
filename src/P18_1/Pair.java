package P18_1;

/**
 * Generic pair class from class example
 * @param <T>
 * @param <S>
 */
public class Pair<T, S>
{
    private T first;
    private S second;
    public Pair(T firstElement, S secondElement)
    {
        first = firstElement;
        second = secondElement;
    }
    public T getFirst() { return first; }
    public S getSecond() { return second; }
}
