/**
 * Cameron Harris
 * Assignment 10
 */
package P18_4;

import java.util.ArrayList;

/**
 * Generic class Measurable
 */
public class Measurable {
    /**
     * Computes the measure of the object.
     *
     * @return the measure
     */
    public double getMeasure() {
        return 0;
    }

    /**
     * Generic Method
     * @param objects ArrayList of objects that extend Measurable
     * @return Largest object in ArrayList
     */
    public static <T extends Measurable> T getLargest(ArrayList<T> objects) {
        T largest = objects.get(0);
        for (T object: objects) {
            if(object.getMeasure() > largest.getMeasure()) {
                largest = object;
            }
        }
        return largest;
    }
}
