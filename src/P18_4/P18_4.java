/**
 * Cameron Harris
 * Assignment 10
 */
package P18_4;

import java.util.ArrayList;

public class P18_4 {
    /**
     * Constructs an ArrayList of type Measurable and calls getLargest method on it
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Measurable> objects = new ArrayList<Measurable>();
        Measurable obj1 = new Measurable() {
            @Override
            public double getMeasure() {
                return 3;
            }
        };
        objects.add(obj1);
        Measurable obj2 = new Measurable() {
            @Override
            public double getMeasure() {
                return 6;
            }
        };
        objects.add(obj2);
        Measurable obj3 = new Measurable() {
            @Override
            public double getMeasure() {
                return 9;
            }
        };
        objects.add(obj3);
        System.out.println("Largest Element: " + Measurable.getLargest(objects).getMeasure());
    }
}
