/**
 * Cameron Harris
 * Assignment 10
 */
package E22_7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Asynchronously counts the number of words in user specified files
 */
public class WordCount {
    private static int threads;
    private static int totalCount;
    private int count;
    private final String fileName;
    private final Lock countLock;

    /**
     * WordCount constructor
     * @param fileName name of file
     */
    public WordCount(String fileName) {
        this.count = 0;
        this.fileName = fileName;
        this.countLock = new ReentrantLock();
    }

    /**
     * Opens a file and counts the number of words
     * @throws FileNotFoundException
     */
    public void readFile() throws FileNotFoundException {
        File myFile = new File(this.fileName);
        Scanner inputFile = new Scanner(myFile);
        while(inputFile.hasNext()) {
            this.count += 1;
            inputFile.next();
        }
        inputFile.close();
    }

    /**
     * Prints the file name and count, if all threads are finished prints the total word count of all files
     */
    public void printCount() {
        System.out.println(this.fileName + ": " + this.count);
        this.countLock.lock();
        totalCount = totalCount + this.count;
        threads = threads - 1;
        this.countLock.unlock();
        if(threads == 0) {
            System.out.println("Combined Count: " + totalCount);
        }
    }

    /**
     * Takes command line arguments and creates threads ti read each file asynchronously
     * @param args file names to be read
     */
    public static void main(String[] args) {
        if(args.length ==  0) {
            System.out.println("No command line arguments!");
        }
        for(String argument: args) {
            WordCount countObject = new WordCount(argument);
            WordCountRunnable runnable = new WordCountRunnable(countObject);
            Thread runnableThread = new Thread(runnable);
            runnableThread.start();
            threads = threads + 1;
        }
    }
}
