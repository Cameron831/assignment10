/**
 * Cameron Harris
 * Assignment 10
 */
package E22_7;

import java.io.FileNotFoundException;

/**
 * Runnable class for WordCount
 */
public class WordCountRunnable implements Runnable{
    private final WordCount countObject;

    /**
     * Constructor for runnable object, receives WordCount object
     * @param countObject WordCount object
     */
    public WordCountRunnable(WordCount countObject) {
        this.countObject = countObject;
    }

    /**
     * Process to be run asynchronously
     */
    @Override
    public void run() {
        try {
            this.countObject.readFile();
            this.countObject.printCount();
        }
        catch (FileNotFoundException exception) {
            System.out.println(exception);
        }
    }
}
